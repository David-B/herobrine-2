package net.theprogrammersworld.herobrine.AI.cores;

import java.util.Objects;
import java.util.stream.Stream;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.block.sign.Side;
import org.bukkit.entity.Player;

import net.theprogrammersworld.herobrine.Herobrine;
import net.theprogrammersworld.herobrine.Support;
import net.theprogrammersworld.herobrine.AI.Core;
import net.theprogrammersworld.herobrine.AI.CoreResult;

public class BuryPlayer extends Core {

	public Block savedBlock1 = null;
	public Block savedBlock2 = null;

	public BuryPlayer() {
		super(CoreType.BURY_PLAYER, AppearType.NORMAL, Herobrine.getPluginCore());
	}

	public CoreResult CallCore(Object[] data) {
		return FindPlace((Player) data[0]);
	}

	public CoreResult FindPlace(final Player player) {
		String resultMessage = null;
			if (!hasNoProtection(player.getLocation())) {
				resultMessage = player.getDisplayName() + " is in a protected area and cannot be buried.";
			} else if (!isEnableBuildTomb(player.getLocation())) {
				resultMessage = player.getDisplayName() + " could not be buried because a good burial location could not be found.";
			} else {
				Bury(player);
			}
		return new CoreResult(resultMessage == null, Objects.toString(resultMessage, player.getDisplayName() + " could not be buried because a good burial location could not be found."));
	}

	public void Bury(Player player) {
		Stream.of(
			player.getLocation().clone().add(0, -1, 0),
			player.getLocation().clone().add(0, -2, 0),
			player.getLocation().clone().add(0, -3, 0),
			player.getLocation().clone().add(0, -1, -1),
			player.getLocation().clone().add(0, -2, -1),
			player.getLocation().clone().add(0, -3, -1)
		).map(Location::getBlock).forEach(Block::breakNaturally);
		player.teleport(player.getLocation().clone().add(0, -3, 0));
		Block signBlock = player.getLocation().clone().add(0, 3, -2).getBlock();
		signBlock.setType(Material.OAK_SIGN);
		((Sign) signBlock.getState()).getSide(Side.FRONT).setLine(1, player.getDisplayName());
		((Sign) signBlock.getState()).update();
		player.getLocation().clone().add(0, 2, 0).getBlock().setType(Material.STONE_BRICKS, false);
		player.getLocation().clone().add(0, 2, -1).getBlock().setType(Material.STONE_BRICKS, false);
	}

	private boolean hasNoProtection(final Location location) {
		final Support support = Herobrine.getPluginCore().getSupport();

		return location != null && Stream.of(location).flatMap((loc) -> Stream.of(/* @formatter:off */
			loc,
			loc.clone().add(0, -1, 0), loc.clone().add(0, -2, 0), loc.clone().add(0, -3, 0),
			loc.clone().add(0, -1, -1), loc.clone().add(0, -2, -1), loc.clone().add(0, -3, -1),
			loc.clone().add(0, -1, -2)
		/* @formatter:on */)).allMatch(support::checkBuild);
    }

	private boolean isEnableBuildTomb(final Location location) {
		return location != null && Stream.of(location).flatMap((loc) -> Stream.of(/* @formatter:off */
			loc.clone().add(0, -1, 0), loc.clone().add(0, -2, 0), loc.clone().add(0, -3, 0),
			loc.clone().add(0, -1, -1), loc.clone().add(0, -2, -1), loc.clone().add(0, -3, -1),
			loc.clone().add(0, -1, -2)
		/* @formatter:on */)).map(Location::getBlock).map(Block::getType).allMatch(Material::isSolid);
	}
}
